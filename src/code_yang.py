import board

from kmk.kmk_keyboard import KMKKeyboard
from kmk.keys import KC
from kmk.hid import HIDModes
from kmk.scanners import DiodeOrientation
from kmk.modules.holdtap import HoldTap
from kmk.modules.layers import Layers
from kmk.modules.split import Split, SplitType

from split_specific import SplitSpecific

keyboard = KMKKeyboard()

keyboard.modules.append(Layers())

holdtap = HoldTap()
keyboard.modules.append(holdtap)

split = Split(split_type=SplitType.BLE, split_target_left=False)
keyboard.modules.append(split)

keyboard.debug_enabled = True

#                       y           u       i           o        p
keyboard.col_pins = (board.D5, board.D2, board.D6, board.D4, board.D3, )
#                       y           h       n           +
keyboard.row_pins = (board.D8, board.D9, board.D7, board.D10, )
keyboard.diode_orientation = DiodeOrientation.ROW2COL


_______ = KC.NO
_TRANS_ = KC.TRNS

LT_1_ESC = KC.HT(KC.ESCAPE, KC.MO(1))
LT_2_SPC = KC.HT(KC.SPACE, KC.MO(2))
LT_3_BSP = KC.HT(KC.BACKSPACE, KC.MO(3))
MT_LGUI_A = KC.HT(KC.A, KC.LGUI)
MT_LALT_R = KC.HT(KC.S, KC.LALT)
MT_LCTRL_S = KC.HT(KC.D, KC.LCTRL)
MT_LSFT_T = KC.HT(KC.F, KC.LSFT)


LT_1_TAB = KC.HT(KC.TAB, KC.MO(1))
LT_2_ENT = KC.HT(KC.ENT, KC.MO(2))
LT_3_DEL = KC.HT(KC.DEL, KC.MO(3))
MT_RSFT_J = KC.HT(KC.J, KC.RSFT)
MT_RCTRL_K = KC.HT(KC.K, KC.RCTRL)
MT_RALT_L = KC.HT(KC.L, KC.RALT)
MT_RGUI_SC = KC.HT(KC.SCOLON, KC.RGUI)


CTRL_TAB = KC.LCTL(KC.TAB)
CL_SH_TAB = KC.LCTL(KC.LSFT(KC.TAB))


keyboard.keymap = [
    #   YIN
    #   LP          LR          LM          LW          LV
    # L0
    [
        KC.Q,       KC.W,       KC.E,       KC.R,       KC.T,           KC.Y,       KC.U,       KC.I,       KC.O,       KC.P,
        MT_LGUI_A,  MT_LALT_R,  MT_LCTRL_S, MT_LSFT_T,  KC.G,           KC.H,       MT_RSFT_J,  MT_RCTRL_K, MT_RALT_L,  MT_RGUI_SC,
        KC.Z,       KC.X,       KC.C,       KC.V,       KC.B,           KC.N,       KC.M,       KC.COMMA,   KC.DOT,     KC.SLASH,
        KC.MO(4),   LT_2_SPC,   LT_1_ESC,   LT_3_BSP,   _______,        _______,    KC.MO(4),   LT_1_TAB,   LT_2_ENT,   LT_3_DEL,
    ],
    # L1
    [
        KC.F4,      KC.F3,      KC.F2,      KC.F1,      KC.AT,          KC.CIRC,    _______,    _______,     _______,    _______,
        KC.F8,      KC.F7,      KC.F6,      KC.F5,      KC.DOLLAR,      KC.AMPR,    KC.HOME,    KC.PGUP,     KC.PGDN,    KC.END,
        KC.F12,     KC.F11,     KC.F10,     KC.F9,      KC.HASH,        KC.PERC,    _______,    _______,     _______,    _______,
        _TRANS_,    _TRANS_,    _TRANS_,     _TRANS_,   _TRANS_,        _______,    _______,    _______,     _______,    _______,
    ],
    # L2
    [
        KC.N4,      KC.N3,      KC.N2,      KC.N1,      KC.MINUS,       KC.PLUS,    _______,    _______,     _______,    _______,
        KC.N8,      KC.N7,      KC.N6,      KC.N5,      KC.TILD,        KC.EQL,     KC.LEFT,    KC.UP,       KC.DOWN,    KC.RIGHT,
        _______,    _______,    KC.N0,      KC.N9,      KC.SLSH,        KC.ASTR,    _______,    _______,     _______,    _______,
        _TRANS_,    _TRANS_,    _TRANS_,     _TRANS_,   _TRANS_,        _______,    _______,    _______,     _______,    _______,
    ],
    # L3
    [
        _______,    _______,    KC.SCLN,    KC.QUES,    _______,        _______,    KC.EXLM,    KC.COLN,     _______,    _______,
        KC.LABK,    KC.LCBR,    KC.LPRN,    KC.LBRC,    KC.BSLS,        KC.SLSH,    KC.RBRC,    KC.RPRN,     KC.RCBR,    KC.RABK,
        _______,    KC.GRV,     KC.QUOT,    KC.DQUO,    _______,        _______,    KC.DQUO,    KC.QUOT,     KC.GRV,     _______,
        _TRANS_,    _TRANS_,    _TRANS_,     _TRANS_,   _TRANS_,        _______,    _______,    _______,     _______,    _______,
    ],
    # L4
    [
        _______,    _______,    CL_SH_TAB,  CTRL_TAB,   KC.PSCR,        _______,    _______,    _______,     _______,    KC.PAUS,
        _______,    _______,    _______,    _______,    _______,        KC.HYPR,    KC.RSFT,    KC.RCTL,     KC.RALT,    KC.RWIN,
        _______,    _______,    _______,    _______,    _______,        _______,    _______,    _______,     _______,    _______,
        _TRANS_,    _TRANS_,    _TRANS_,     _TRANS_,   _TRANS_,        _______,    _______,    _______,     _______,    _______,
    ],
]


if __name__ == '__main__':
    keyboard.go(hid_type=HIDModes.BLE, ble_name="YinYang")

