#   when running in safemode, copy-paste these into the REPL terminal to see more of the reason
# import microcontroller
# import supervisor
# supervisor.runtime.safe_mode_reason

#  and copy-paste this one to get out of safemode
# microcontroller.reset()

#  or, for short:
# import  microcontroller
# microcontroller.reset()
