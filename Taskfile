#!/bin/bash
set -eo pipefail

# Add things to your PATH for this file only
PATH=./src/vendor/bin:./src/node_modules/.bin:$PATH

export VOLUME_BASE_PATH="/media/johan"
export TARGET_VOLUME_LEFT="$VOLUME_BASE_PATH/YINL"
export TARGET_VOLUME_RIGHT="$VOLUME_BASE_PATH/YANGR"

function task:watch { ## Keep code.py synced to both target volumes
  echo "src/code.py" | entr -p ./Taskfile copy /_
#  ls src/*.py | entr -p ./Taskfile copy /_
}

function task:copy { ## Copy a file to both target volumes
  SUBJECT_FILE="$1"

  if [ -e $TARGET_VOLUME_LEFT ]; then
    CODE_FILENAME=$(basename "$SUBJECT_FILE")
    cp -a "$SUBJECT_FILE" "$TARGET_VOLUME_LEFT/$CODE_FILENAME"
  fi

  if [ -e $TARGET_VOLUME_RIGHT ]; then
    CODE_FILENAME=$(basename "$SUBJECT_FILE")
    cp -a "$SUBJECT_FILE" "$TARGET_VOLUME_RIGHT/$CODE_FILENAME"
  fi
}

function task:term-list {
  ls -al /dev/ttyACM*
}

function task:term-attach {
  TERM_PATH="/dev/ttyACM$1"

  if [ -e $TERM_PATH ]; then
    screen -D -R "johan-$1" $TERM_PATH 115200
  else
    echo "'$TERM_PATH' not found."
    exit 1;
  fi
}

function task:kmk-update {
  git submodule update lib/kmk_firmware
}

function task:kmk-build {
  echo "Building KMK"
  ORIG_DIR=$(pwd)

  cd lib/kmk_firmware \
    && rm -rf .compiled \
    && make compile

  cd $ORIG_DIR
}

function task:format { ## Explains how to format a nicenano disk the right way
  echo "do this:"
  echo "sudo mkfs.vfat -s 1 -S 512 -n YINL /dev/sda1"
  echo "(find which /dev in the Disks app)"
}

function task:kmk-copy {
  task:kmk-build
  echo "TODO: onderstaande met rsync doen, gaat veel sneller :-/"
  kmk-cleanup

  echo "Copying KMK to keyboard"
  SOURCE="./lib/kmk_firmware/.compiled/kmk"
  if [ -e $TARGET_VOLUME_LEFT ]; then
    cp -a $SOURCE $TARGET_VOLUME_LEFT
  fi

  if [ -e $TARGET_VOLUME_RIGHT ]; then
    cp -a $SOURCE $TARGET_VOLUME_RIGHT
  fi
}

function kmk-cleanup {
  echo "Cleaning up existing KMK dir"
  if [ -e "$TARGET_VOLUME_LEFT/kmk" ]; then
    rm -rf "$TARGET_VOLUME_LEFT/kmk"
  fi

  if [ -e "$TARGET_VOLUME_RIGHT/kmk" ]; then
    rm -rf "$TARGET_VOLUME_RIGHT/kmk"
  fi
}

# ===========================
# Default: help section
# ===========================

function task:help { ## This help text
  echo "Tasks:"
  cat $0 | awk 'match($0, /^function\s+task:([a-z][a-zA-Z0-9_:-]*)([ ]+\{[ ]*##(.*))*/, a) {printf "  \033[33m%-15s\033[0m  %s\n", a[1], a[3]}'
}

"task:${@:-help}"

